const {
  DB_USER = "postgres",
  DB_PASSWORD = "Koki12001",
  DB_NAME = "bcr",
  DB_HOST = "127.0.0.1",
  DB_PORT = "5432",
} = process.env;


module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: "final_dev",
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres",
    // "logging": false,
  },
  test: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: "final_dev",
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres",
    // "logging": false,
  },
  production: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: "d8snv34n5akknm",
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres",
    // "logging": false,
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      }
    }
  }
}