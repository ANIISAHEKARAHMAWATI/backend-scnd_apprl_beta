const express = require("express");
const controllers = require("../app/controllers");
const upload = require("../app/cloudinary/upload");
const apiRouter = express.Router();
const { body, validationResult } = require('express-validator')
const paginate = require('express-paginate');
const cors = require("cors");
// const { Server } = require("socket.io");
// const server = http.createServer(app);

// const io = new Server(server, {
//     cors: {
//       origin: "localhost:8000/",
//     //   origin: "https://frontend-scnd-apprl-beta-4.vercel.app/", // TODO: Ganti jadi URL react-mu
//       methods: ["GET", "POST"],
//     },
// });

// io.on("connection", (socket) => {
//     console.log(`User Connected: ${socket.id}`);

//     socket.on("notif", (data) => {
//         socket.join(data);
//         console.log(`User with ID: ${socket.id} joined room: ${data}`);
//     });

//     socket.on("send_notif", (data) => {
//         socket.to(data.room).emit("receive_message", data);
//     });

//     socket.on("disconnect", () => {
//         console.log("User Disconnected", socket.id);
//     });
// });
  
// server.listen(8000, () => {
//     console.log("SERVER RUNNING");
// });
  

apiRouter.use(paginate.middleware(10, 50));

// Swagger
const swaggerUI = require("swagger-ui-express");
const swaggerDocument = require("../swagger.json");
const { application } = require("express");
apiRouter.use("/swg", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

//home
apiRouter.get("/home", controllers.api.v1.appController.getRoot), //for testing
apiRouter.get("/", controllers.api.v1.productController.list)

//login and register
apiRouter.post("/register", body('email').isEmail(), body('password').isLength({ min: 6 }), controllers.api.v1.userController.create);
apiRouter.post("/login", controllers.api.v1.userController.login); 

//cek user khusus admin
apiRouter.get("/user", controllers.api.v1.userController.authorize, controllers.api.v1.userController.list);

// profile
apiRouter.get("/profile", controllers.api.v1.userController.authorize, controllers.api.v1.userController.profile);
apiRouter.post("/profile", upload.single("picture"), controllers.api.v1.userController.update);
 
//buyer & seller
apiRouter.get("/products", controllers.api.v1.userController.authorize, controllers.api.v1.productController.list);
apiRouter.post("/products/search", controllers.api.v1.userController.authorize, controllers.api.v1.productController.search);
apiRouter.post("/products/filter", controllers.api.v1.userController.authorize, controllers.api.v1.productController.sorting);
apiRouter.get("/product/:id", controllers.api.v1.userController.authorize, controllers.api.v1.productController.show);

//seller 
apiRouter.get("/products/seller", controllers.api.v1.userController.authorize, controllers.api.v1.productController.list_seller);
apiRouter.post("/product/create", controllers.api.v1.userController.authorize, controllers.api.v1.productController.create, upload.array("picture", 4), controllers.api.v1.productController.createproduct);
apiRouter.put("/product/:id", controllers.api.v1.userController.authorize, controllers.api.v1.productController.update);
// apiRouter.delete("/product/:id", controllers.api.v1.userController.authorize, controllers.api.v1.productController.destroy);

// Buyer Transaction
apiRouter.get("/transaction/all", controllers.api.v1.userController.authorize, controllers.api.v1.transactionController.list); 
apiRouter.get("/transaction", controllers.api.v1.userController.authorize, controllers.api.v1.transactionController.list_transaction);   //list hisotry penawaran bersadarkan user sendiri gunakan auth
apiRouter.post("/transaction", controllers.api.v1.userController.authorize, controllers.api.v1.transactionController.create);
apiRouter.get("/transaction/:id", controllers.api.v1.userController.authorize, controllers.api.v1.transactionController.show);
// apiRouter.delete("/transaction/:id", controllers.api.v1.userController.authorize, controllers.api.v1.transactionController.destroy);

//Final transaction (seller)
apiRouter.get("/products/soldout", controllers.api.v1.userController.authorize, controllers.api.v1.productController.list_soldout);
apiRouter.put("/confirm", controllers.api.v1.userController.authorize, controllers.api.v1.transactionController.update);

//Notif
apiRouter.get("/notifs", controllers.api.v1.userController.authorize, controllers.api.v1.notifController.list);
apiRouter.post("/notif/detail", controllers.api.v1.userController.authorize, controllers.api.v1.notifController.show);
apiRouter.post("/notif", controllers.api.v1.userController.authorize, controllers.api.v1.notifController.create);

// apiRouter.get("/api/v1/errors", () => {
//   throw new Error(
//     "The Industrial Revolution and its consequences have been a disaster for the human race."
//   );
// });

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
