// Integration Test and Unit Test
// Containing fail test only

const { response } = require('express');
const request = require('supertest');
const app = require('../app/index');

jest.setTimeout(5000);

describe('Buyer API', () => {
    it('Fail Test Register API -> POST /', () => {
        return request(app)
            .post("/register")
            .send({
                userName: "Alexmo Swagger",
                email: "alexmoswagger@gmail.com",
                password: "12345",
            })
            .expect(400)
    })
    // it('Fail Test Register API 2-> POST /', () => {
    //     return request(app)
    //         .post("/register")
    //         .send({
    //             userName: "Alexmo Swagger",
    //             email: "alexmoswagger@binar.id",
    //             password: "12345",
    //         })
    //         .expect(400)
    // })
    // it('Fail Test Login API -> POST /', () => {
    //     return request(app)
    //         .post("/login")
    //         .send({
    //             email: "mr.exampl@gmail.com",
    //             password: "12345",
    //         })
    //         .expect(404)
    // })
    // it('Fail Test Login API 2 -> POST /', () => {
    //     return request(app)
    //         .post("/login")
    //         .send({
    //             email: "mr.example@gmail.com",
    //             password: "1234",
    //         })
    //         .expect(401)
    // })

    it("Fail Test Show Product API => GET /", () => {
        return request(app)
            .get("/products")
            .query({
                limit: "0",
                page: "1",
            })
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .expect("Content-Type", /json/)
            .expect(400)
    })

    it("Fail Test Create Product API (max create)=> POST /", () => {
        // const filePath = `${__dirname}/test_image/nike.jpg`;
        // return request(app)
        //     .post("/product/create")
        //     .set(
        //         "Authorization",
        //         // Token Seller Mr.HelloWorld
        //         "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
        //     )
        //     .send(
        //         {
        //             productName: "Nike 2020",
        //             productPrice: 150000,
        //             productDesc: "Nike Pendek",
        //             productCategory: "celana",
        //             picture: filePath
        //         }
        //     )
        //     // .field("productName", JSON.stringify({ name: "Geoff Max" }))
        //     // .attach("picture", filePath)
        //     .expect(422)
    })
});
