// Integration Test and Unit Test
// Containing success test only

const { response } = require('express');
const request = require('supertest');
const app = require('../app/index');
// const product = require('../app/models/product');

// set test timeout
jest.setTimeout(10000);

describe("App API Kit", () => {
    it('Test Handle API at Start -> GET /', () => {
        return request(app)
            .get("/home")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).toEqual({
                    status: "OK",
                    message: "SH API running!",
                });
            });
    })

    it('Test Landing Page at Start -> GET /', () => {
        return request(app)
            .get("/")
            .query({
                limit: "1",
                page: "1",
            })
            .expect(200)
    })

    it('Test Landing Page at Start -> GET /', () => {
        return request(app)
            .get("/user")
            .set(
                "Authorization",
                // Token Seller Mr.HelloWorld
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .expect(200)
    })
}
)

describe("Buyer API", () => {

    // success
    // it('Test Register API -> POST /', () => {
    //     return request(app)
    //         .post("/register")
    //         .send({
    //             userName: "rickys1",
    //             email: "test1@gmail.com",
    //             password: "test123!",
    //         })
    //         .expect(201)
    // })

    it('Test Login API -> POST /', () => {
        return request(app)
            .post("/login")
            .send({
                email: "ricky@gmail.com",
                password: "ricky123",
            })
            .expect(200)
            .then((response) => {
                expect(response.body).toHaveProperty('token');
            });
    })

    it("Test Get Profile API => GET /", () => {
        return request(app)
            .get("/profile")
            .set(
                "Authorization",
                //Token Buyer Mrs.Jane
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .expect("Content-Type", /json/)
            .expect(200)
    })

    // it("Test Update Profile API => GET /", () => {
    //     return request(app)
    //         .post("/profile")
    //         .set(
    //             "Authorization",
    //             //Token Buyer Mrs.Jane
    //             "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwibmFtZSI6Ik1ycy4gSmFuZSIsImltYWdlIjoiaHR0cHM6Ly9pbWFnZXMucGV4ZWxzLmNvbS9waG90b3MvMTIzOTI5MS9wZXhlbHMtcGhvdG8tMTIzOTI5MS5qcGVnP2NzPXNyZ2ImZGw9cGV4ZWxzLWRhbmllbC14YXZpZXItMTIzOTI5MS5qcGcmZm09anBnIiwiZW1haWwiOiJtcnMuamFuZUBnbWFpbC5jb20iLCJyb2xlIjoiQnV5ZXIiLCJpYXQiOjE2NTcwODIxNzl9.yFtUB9hIiv4C-YZmao4wvAdfHAJi2H_FSEQaZDI4OSA"
    //         )
    //         .attach("picture", "../uploads/test-img.jpg")
    //         .expect("Content-Type", /json/)
    //         .expect(200)
    // })

    it("Test Show Product API => GET /", () => {
        return request(app)
            .get("/products")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .expect("Content-Type", /json/)
            .expect(200)
    })

    it("Test Search Product API => POST /", () => {
        return request(app)
            .post("/products/search")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .query({
                limit: "1",
                page: "1",
            })
            .send({
                search: "sepatu"
            })
            .expect("Content-Type", /json/)
            .expect(200)
    })

    it("Test Search Product API with empty value => POST /", () => {
        return request(app)
            .post("/products/search")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .query({
                limit: "1",
                page: "1",
            })
            .send({
                search: ""
            })
            .expect("Content-Type", /json/)
            .expect(200)
    })

    it("Test Filter Product API => POST /", () => {
        return request(app)
            .post("/products/filter")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .query({
                limit: "1",
                page: "1",
            })
            .send({
                category: "sepatu"
            })
            .expect("Content-Type", /json/)
            .expect(200)
    })

    it("Test Show Transaction => GET /", () => {
        return request(app)
            .get("/transaction")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .expect("Content-Type", /json/)
            .expect(200)
    })


    // it("Test Create Transaction => POST /", () => {
    //     return request(app)
    //         .post("/transaction")
    //         .set(
    //             "Authorization",
    //             "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
    //         )
    //         .send({
    //             productId : 2,
    //             priceNegotiate : 600000
    //         })
    //         .expect("Content-Type", /json/)
    //         .expect(201)
    // })

    // success
    it("Test Show Transaction => GET /", () => {
        return request(app)
            .get("/transaction/2")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .expect(200)
    })



})

describe('Seller API', () => {
    it("Test Create Product API => POST /", () => {
        // const filePath = `../test_image/nike.jpg`;
        return request(app)
            .post("/product/create")
            .set(
                "Authorization",
                // Token Seller Mr.HelloWorld
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywibmFtZSI6InphbmkiLCJpbWFnZSI6bnVsbCwiZW1haWwiOiJ6YW5vQGdtYWlsLmNvbSIsInJvbGUiOiJidXllciIsImlhdCI6MTY1ODY1Njc5MiwiZXhwIjoxNjU4NzQzMTkyfQ.eQj5hxJk-Ih1XaAR3V3E_Uf_D2d-qV9voVoIYPnNBzA"
            )
            .field({
                productName: "Nike 2020",
                productPrice: 150000,
                productDesc: "Nike Pendek",
                productCategory: "celana",
            })
            // .field('productPrice', '150000')
            // .field('productDesc', 'Nike Pendek')
            // .field('productCategory', 'celana')
            .attach(
                // {
                //     productName: "Nike 2020",
                //     productPrice: 150000,
                //     productDesc: "Nike Pendek",
                //     productCategory: "celana",
                    'picture', '/nike.jpg'
                // }
            )
            .expect(201)
    })

    // it("Test Update Product API => PUT /", () => {
    //     return request(app)
    //         .put("/product/6")
    //         .set(
    //             "Authorization",
    //             // Token Seller Mr.HelloWorld
    //             "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwibmFtZSI6Ik1yLiBIZWxsb1dvcmxkIiwiaW1hZ2UiOiJodHRwczovL2ltYWdlcy5wZXhlbHMuY29tL3Bob3Rvcy8xMjM5MjkxL3BleGVscy1waG90by0xMjM5MjkxLmpwZWc_Y3M9c3JnYiZkbD1wZXhlbHMtZGFuaWVsLXhhdmllci0xMjM5MjkxLmpwZyZmbT1qcGciLCJlbWFpbCI6Im1yLmhlbGxvd29ybGRAZ21haWwuY29tIiwicm9sZSI6IlNlbGxlciIsImlhdCI6MTY1ODQxNDg5NCwiZXhwIjoxNjU4NTAxMjk0fQ.gQ6hjW62ROET94lq60DLLRFera8Xe4VC5Ch-6t3GkcU"
    //         )
    //         .send({
    //             productName: "EIGER EMISSARY 3L WAIST BAG",
    //             productPrice: 250000,
    //             productDesc: "Lorem ipsum dolor sit amet."
    //         })
    //         .expect(200)
    // })

    // it("Test Get Product API => GET /", () => {
    //     return request(app)
    //         .get("/product/19")
    //         .set(
    //             "Authorization",
    //             // Token Seller Mr.HelloWorld
    //             "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwibmFtZSI6Ik1yLiBIZWxsb1dvcmxkIiwiaW1hZ2UiOiJodHRwczovL2ltYWdlcy5wZXhlbHMuY29tL3Bob3Rvcy8xMjM5MjkxL3BleGVscy1waG90by0xMjM5MjkxLmpwZWc_Y3M9c3JnYiZkbD1wZXhlbHMtZGFuaWVsLXhhdmllci0xMjM5MjkxLmpwZyZmbT1qcGciLCJlbWFpbCI6Im1yLmhlbGxvd29ybGRAZ21haWwuY29tIiwicm9sZSI6IlNlbGxlciIsImlhdCI6MTY1NzYyMjI2Mn0.Ha005bYFM9IazJzkJpom2b2O3EI0miThW84xBBK3dKU"
    //         )
    //         .expect("Content-Type", /json/)
    //         .expect(200)
    // })
    // Success
    // it("Test Delete Product API => DELETE /", () => {
    //     return request(app)
    //         .delete("/product/23")
    //         .set(
    //             "Authorization",
    //             // Token Seller Mr.HelloWorld
    //             "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwibmFtZSI6Ik1yLiBIZWxsb1dvcmxkIiwiaW1hZ2UiOiJodHRwczovL2ltYWdlcy5wZXhlbHMuY29tL3Bob3Rvcy8xMjM5MjkxL3BleGVscy1waG90by0xMjM5MjkxLmpwZWc_Y3M9c3JnYiZkbD1wZXhlbHMtZGFuaWVsLXhhdmllci0xMjM5MjkxLmpwZyZmbT1qcGciLCJlbWFpbCI6Im1yLmhlbGxvd29ybGRAZ21haWwuY29tIiwicm9sZSI6IlNlbGxlciIsImlhdCI6MTY1NzYyMjI2Mn0.Ha005bYFM9IazJzkJpom2b2O3EI0miThW84xBBK3dKU"
    //         )
    //         .expect(204)
    // })

    // it("Test Filter Transaction API => GET /", () => {
    //     return request(app)
    //         .get("/transaction/seller/4")
    //         .set(
    //             "Authorization",
    //             // Token Seller Mr.HelloWorld
    //             "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwibmFtZSI6Ik1yLiBIZWxsb1dvcmxkIiwiaW1hZ2UiOiJodHRwczovL2ltYWdlcy5wZXhlbHMuY29tL3Bob3Rvcy8xMjM5MjkxL3BleGVscy1waG90by0xMjM5MjkxLmpwZWc_Y3M9c3JnYiZkbD1wZXhlbHMtZGFuaWVsLXhhdmllci0xMjM5MjkxLmpwZyZmbT1qcGciLCJlbWFpbCI6Im1yLmhlbGxvd29ybGRAZ21haWwuY29tIiwicm9sZSI6IlNlbGxlciIsImlhdCI6MTY1ODQxNDg5NCwiZXhwIjoxNjU4NTAxMjk0fQ.gQ6hjW62ROET94lq60DLLRFera8Xe4VC5Ch-6t3GkcU"
    //         )
    //         .expect(200)
    // })

    it("Test Show Sold Product Transaction API => GET /", () => {
        return request(app)
            .get("/products/seller")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .expect("Content-Type", /json/)
            .expect(200)
    })

    it("Test Show Sold Product Transaction API => GET /", () => {
        return request(app)
            .get("/products/soldout")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .expect("Content-Type", /json/)
            .expect(200)
    })

    it("Test Update Transaction API => POST /", () => {
        return request(app)
            .put("/confirm")
            .set(
                "Authorization",
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IlJpY2FyZG8iLCJpbWFnZSI6Imh0dHA6Ly9yZXMuY2xvdWRpbmFyeS5jb20vbGVvemFuLTgwOTAvaW1hZ2UvdXBsb2FkL3YxNjU4NDY0NDY1L2lta3RuaHFtaDI4enhjOTZiZmVhLmpwZyIsImVtYWlsIjoicmlja3lAZ21haWwuY29tIiwicm9sZSI6InNlbGxlciIsImlhdCI6MTY1ODU5NDU3NiwiZXhwIjoxNjU4NjgwOTc2fQ.GZ4qQrXlpyf36LFA8K0-ao-NvUY2T8oyDarKj3gPT5w"
            )
            .send({
                id: "1",
                transactionStatus: "accept"
            })
            .expect("Content-Type", /json/)
            .expect(200)
    })
});
