'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Notif extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Notif.belongsTo(models.Product,{
        foreignKey:"productId"
      });

      Notif.belongsTo(models.User_Transaction,{
        foreignKey:"transactionId"
      });

      Notif.belongsTo(models.User,{
        foreignKey:"buyerId"
      });
    }
  }
  Notif.init({
    sellerId: DataTypes.INTEGER,
    buyerId: DataTypes.INTEGER,
    productId: DataTypes.INTEGER,
    transactionId: DataTypes.INTEGER,
    message: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Notif',
  });
  return Notif;
};