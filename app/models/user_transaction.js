'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_Transaction.belongsTo(models.Product,{
        foreignKey:"productId"
      });

      User_Transaction.belongsTo(models.User,{
        foreignKey:"buyerId"
      });

      User_Transaction.belongsTo(models.Image_Product,{
        foreignKey:"productId"
      })
    }
  }
  User_Transaction.init({
    productId: DataTypes.INTEGER,
    buyerId: DataTypes.INTEGER,
    sellerId: DataTypes.INTEGER,
    priceNegotiate: DataTypes.INTEGER,
    transactionStatus: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_Transaction',
  });
  return User_Transaction;
};