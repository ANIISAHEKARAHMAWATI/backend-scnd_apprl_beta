'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Image_Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Image_Product.belongsTo(models.Product,{
        foreignKey:"productId"
      });

      Image_Product.belongsTo(models.User_Transaction,{
        foreignKey:"productId"
      });
    }
  }
  Image_Product.init({
    productId: DataTypes.INTEGER,
    link: DataTypes.STRING  
  }, {
    sequelize,
    modelName: 'Image_Product',
  });
  return Image_Product;
};