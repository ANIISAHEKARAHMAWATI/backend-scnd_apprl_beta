const productRepo = require("../repositories/productRepo");

module.exports = {
  create(requestBody) {
    return productRepo.create(requestBody);
  },

  update(id, requestBody) {
    return productRepo.update(id, requestBody);
  },

  // delete(id) {
  //   return productRepo.delete(id);
  // },

  async list(token) {
    try {
      // console.log(token)
      const posts = await productRepo.findAll(parseInt(token));

      return {
        data: posts,
        // count: postCount
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return productRepo.find(id);
  },

  cek(requestBody){
    return productRepo.cek(requestBody);
  },

  //filter search by name
  searchName(requestBody, limit, page){
    return productRepo.search(requestBody, parseInt(limit), parseInt(page))
  },

  //filter category
  sorting(requestBody, limit, page){
    return productRepo.sort(requestBody, parseInt(limit), parseInt(page))
  },

  getAll(limit, page){
    return productRepo.allData(parseInt(limit), parseInt(page))
  },

  getHistory(token){
    return productRepo.history(token)
  }

};
