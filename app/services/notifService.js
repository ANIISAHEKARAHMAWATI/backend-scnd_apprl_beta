const { Op, where } = require("sequelize");
const { sequelize } = require("../models");
const notifRepo = require("../repositories/notifRepo");

module.exports = {
  create(requestBody) {
    return notifRepo.create(requestBody);
  },

  async listall() {
    try {
      const posts = await notifRepo.findAll();
      // const postCount = await notifRepo.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return notifRepo.find(id);
  },
};
