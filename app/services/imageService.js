const imageRepo = require("../repositories/imageRepo");

module.exports = {
  create(requestBody) {
    return imageRepo.create(requestBody);
  },

  delete(id) {
    return imageRepo.delete(id);
  },
};
