const userRepo = require("../repositories/userRepo");

module.exports = {
  create(requestBody) {
    return userRepo.create(requestBody);
  },

  login(requestBody){
    return userRepo.login(requestBody);
  },

  update(id, requestBody) {
    return userRepo.update(id, requestBody);
  },

  delete(id) {
    return userRepo.delete(id);
  },

  async list() {
    try {
      const posts = await userRepo.findAll();
      const postCount = await userRepo.getTotalPost();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return userRepo.find(id);
  },

  async checknew(email){
    const data = await userRepo.login(email);
    return {data};
  }
};
