const { Op, where } = require("sequelize");
const { sequelize } = require("../models");
const transactionRepo = require("../repositories/transactionRepo");

module.exports = {
  create(requestBody) {
    return transactionRepo.create(requestBody);
  },

  update(id, requestBody) {
    return transactionRepo.update(id, requestBody);
  },

  delete(id) {
    return transactionRepo.delete(parseInt(id));
  },

  async listall(limit, page) {
    try {
      const posts = await transactionRepo.findAll(parseInt(limit), parseInt(page));
      // const postCount = await transactionRepo.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return transactionRepo.find(id);
  },

  getid(id){
    return transactionRepo.lastid(parseInt(id))
  },

  list_transaction(id){
    return transactionRepo.listTransaction(id)
  },

  cek(id){
    return transactionRepo.cek(id);
  }
};
