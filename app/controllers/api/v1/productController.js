const productService = require("../../../services/productService");
const imageService = require("../../../services/imageService");
const jwt = require("jsonwebtoken")
const SALT = 10;
const {uploadToCloudinary} = require("../../../cloudinary/cloudinary");
const { extname } = require('path')
// const upload = require("../../../cloudinary/upload")

function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia");
}

module.exports = {
  async list(req, res) {
    // try {
      const {limit} = req.query; // Make sure to parse the limit to number
      const {page} = req.query
      if (!(limit >= 1 && page >= 1)){
        return res
        .status(400)
        .json({
            message: "Bad Request",
          });
      }
      
      const data = await productService.getAll(limit, page);
      return res.status(200).json({
        status : "OK",
        data,
        meta: { 
          total : data.length, 
          page : req.query.page
        }
      });
    // }catch (err) {
    //   console.error(err);
    //   res.status(400).json({
    //     message: "Bad Request",
    //   });
    // }
  },

  async search(req, res) {
    const search = req.body.search.toUpperCase()

    const {limit} = req.query; // Make sure to parse the limit to number
    const {page} = req.query;
    
    if (!search) {
      const find = await productService.getAll(limit, page);
      return res.status(200).json({
        status : "OK",
        find,
        meta: { 
          total : find.length, 
          page : req.query.page
        }
      });
    }

    // try {
      const find = await productService.searchName(search, limit, page)
      // console.log(find)
      if (find){
        res.status(200).json({
          status: "OK",
          find,
          meta: { 
            total : find.length, 
            page : req.query.page
          }
        });
        
      }
    // } catch (err) {
    //   console.error(err);
    //   res.status(401).json({
    //     message: "Data not Found",
    //   });
    // }
  },

  async sorting(req, res) {
    const search = req.body.category.toUpperCase()
    const {limit} = req.query; // Make sure to parse the limit to number
    const {page} = req.query;
    // try {
      const find = await productService.sorting(search, limit, page)
      // console.log(find)
      if (find){
        res.status(200).json({
          status: "OK",
          find,
          meta: { 
            total : find.length, 
            page : req.query.page
          }
        });
      }
    // } catch (err) {
    //   console.error(err);
    //   res.status(401).json({
    //     message: "Data not Found",
    //   });
    // }
  },

  list_seller(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );

    // if (tokenPayload.role == "buyer") {
    //   res.status(403).json({ message: "Hak akses Seller" });
    //   return;
    // }
    // console.log(tokenPayload)
    productService
      .list(tokenPayload.id)
      .then(({ data }) => {
        // const newdata = data.filter((row) => row.userId == tokenPayload.id);
        // const newCount = newdata.length;
        res.status(200).json({
          status: "OK",
          data,
          meta: data.length,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async list_soldout(req, res){
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );

    // const {limit} = req.query; // Make sure to parse the limit to number
    // const {page} = req.query;
    // if (!(limit >= 1 && page >= 1)){
    //   return res
    //    .status(400)
    //    .json({
    //       message: "Bad Request",
    //     });
    // }
    // try {
      const data = await productService.getHistory(tokenPayload.id);
      return res.status(200).json({
        status : "OK",
        data,
        meta: { 
          total : data.length, 
          page : req.query.page
        }
      });
    // }catch (err) {
    //   console.error(err);
    //   res.status(400).json({
    //     message: "Bad Request",
    //   });
    // }
  },

  create(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );
      productService
        .list(tokenPayload.id)
        .then(({ data }) => {
          // const newdata = data.filter((row) => row.userId == tokenPayload.id);
          // const filterdata = data.filter((row) => row.product_status == "available");
          // newCount = filterdata.length;
          console.log(data.length)
          if (data.length < 4) {
            next()
          } else {
            res.status(422).json({
              message: "Jumlah Maksimal post 4",
            });
          }
        })
    } catch (err) {
      console.error(err);
      res.status(401).json({
        message: "Unauthorized",
      });
    }

  },
 
  async createproduct(req, res) {
    try{
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );
  
      const name = req.body.productName
      const productName = name.toUpperCase()
      const productPrice = req.body.productPrice
      const productDesc = req.body.productDesc
      const category = req.body.productCategory
      const productCategory = category.toUpperCase()
  
      const detail_product = await productService.create({productName, productPrice, productDesc, productCategory, userId: tokenPayload.id, productStatus: "available" })
      console.log(detail_product)
      for (var i = 0; i < req.files.length; i++) {
        var locaFilePath = req.files[i].path;
        cekfile = extname(locaFilePath).toString()
        // console.log(cekfile)
        if(cekfile != ".jpg"){
          if(cekfile != ".png"){
            if(cekfile != ".jpeg"){
              res.status(400).json({message: "Not image file"});
              fs.unlinkSync(locaFilePath);
              return;
            }
          }
        }
        // Upload the local image to Cloudinary
        // and get image url as response
        var result = await uploadToCloudinary(locaFilePath);
        let imgtostring = result.url.toString();
        const create_image = await imageService.create({
          productId:detail_product.id,
          link:imgtostring
        })
        var locaFilePath = "";
      }
    res.status(201).json({
      status: "OK",
      data: detail_product
    });
    }catch(err){
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
    };
  },

  async update(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );

    // if (tokenPayload.role == "buyer") {
    //   res.status(403).json({ message: "Seller access" });
    //   return;
    // }

    const idProduct = req.params.id
    const cekproduct = await productService.cek(idProduct)
    // console.log(cekproduct)
    if (cekproduct.user_id != tokenPayload.id){
      res.status(403).json({ message: "Does not have access" });
      return;
    }

    productService
      .update(req.params.id, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  //by id
  show(req, res) {
    productService
      .get(req.params.id)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  //delete
  // async destroy(req, res) {
  //   const bearerToken = req.headers.authorization;
  //   const token = bearerToken.split("Bearer ")[1];
  //   const tokenPayload = jwt.verify(
  //     token,
  //     process.env.JWT_SIGNATURE_KEY || "Rahasia"
  //   );

  //   // if (tokenPayload.role == "buyer") {
  //   //   res.status(403).json({ message: "Seller access" });
  //   //   return;
  //   // }

  //   const idProduct = req.params.id
  //   const cekproduct = await productService.cek(idProduct)

  //   if (cekproduct.userId != tokenPayload.id){
  //     res.status(403).json({ message: "Does not have access" });
  //     return;
  //   }

  //   productService
  //     .delete({
  //       where: {
  //         id: req.params.id
  //       }
  //     })
  //     .then(() => {
  //       res.status(204).end();
  //     })
  //     .catch((err) => {
  //       res.status(422).json({
  //         status: "FAIL",
  //         message: err.message,
  //       });
  //     });
  // },
};
