const jwt = require("jsonwebtoken") 
const transactionService = require("../../../services/transactionService");
// const notifService =require("../../../services/notifService");
const productService = require("../../../services/productService");
const { hasNextPages } = require("express-paginate");

module.exports = { 
  list(req, res) {
    const id = req.params.id;
    const {limit} = req.query; // Make sure to parse the limit to number
    const {page} = req.query
    transactionService
      .listall(limit, page)
      .then(({ data }) => {
        res.status(200).json({
          status: "OK",
          data,
          meta: { 
            total: data.length,
            page : req.query.page 
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  //menampilkan list transaction untuk user id
  async list_transaction(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );

    // const {limit} = req.query; // Make sure to parse the limit to number
    // const {page} = req.query

    // const data = await transactionService.list_transaction(limit, page, tokenPayload.id);
    const data = await transactionService.list_transaction(tokenPayload.id);
    return res.status(200).json({
      status : "OK",
      data,
      meta: { 
        total : data.length, 
        page : req.query.page
      }
    });
  },

  // create transaction and create notif 
  async create(req, res) {
    try{
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );

      // get data penawarann
      const buyerId = tokenPayload.id
      const productId = req.body.productId
      const priceNegotiate = req.body.priceNegotiate
      const transactionStatus = "waiting"
      
      const find = await productService.cek(productId) //sudah dengan format json
      // const data = await find.json()
      // console.log(find.user_id)
      if(!find){
        res.status(401).json({
          message: "Data not Found",
        });
      }else{
        const sellerId = find.user_id
        let data = { productId, buyerId, sellerId, priceNegotiate, transactionStatus}
    
        let upload = await transactionService.create(data)
        // console.log(tokenPayload)
        res.status(201).json({
          status: "Penawaran Berhasil Dibuat",
          upload
        });
      }

    }catch(err){
      res.status(422).json({
        status: "FAIL",
        message: err.message,
      });
    };
  },

  async update(req, res) {
    //id transaction yang di update
    const id = req.body.id
    const find = await transactionService.cek(id) //sudah dengan format json
    const productId = find.product_id
    const transactionStatus = req.body.transactionStatus
    let productStatus 
    
    if (transactionStatus=="accept"){
      productStatus = "soldout"
    }else{
      productStatus = "available"
    }
    console.log(productStatus)
    const updatetransaction = await transactionService.update(id,{transactionStatus})
    const updateProduct = await productService.update(productId, {productStatus})

    res.status(200).json({
      status:"OK"
    })
    // transactionService
    //   .update(id,{transactionStatus})
    //   .then(() => {
    //     res.status(200).json({
    //       status: "OK",
    //     });
    //   })
    //   .catch((err) => {
    //     res.status(422).json({
    //       status: "FAIL",
    //       message: err.message,
    //     });
    //   });
    
    // productService
    //   .update(productId, productStatus)
    //   .then(() => {
    //     res.status(200).json({
    //       status: "OK",
    //     });
    //   })
    //   .catch((err) => {
    //     res.status(422).json({
    //       status: "FAIL",
    //       message: err.message,
    //     });
    //   });
  },

  // getMaxTransaction(req, res) {
  //   const bearerToken = req.headers.authorization;
  //   const token = bearerToken.split("Bearer ")[1];
  //   const tokenPayload = jwt.verify(
  //     token,
  //     process.env.JWT_SIGNATURE_KEY || "Rahasia"
  //   );

  //   if (tokenPayload.role == "buyer") {
  //     res.status(403).json({ message: "Hak akses Seller" });
  //     return;
  //   }

  //   transactionService
  //     .list()
  //     .then(({ data }) => {
  //       const newdata = data.filter((row) => row.productId == req.params.id);
  //       let max = Math.max(...newdata.map(({ priceNegotiate }) => priceNegotiate));
  //       const finalTransaction = newdata.filter((row) => row.priceNegotiate == max);

  //       res.status(200).json({
  //         status: "OK",
  //         max_price: max,
  //         finalTransaction
  //       });
  //       console.log(newdata);
  //     })
  // },

  //by id
  show(req, res) {
    transactionService
      .get(req.params.id)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          post
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  //delete
  destroy(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );

    id = req.params.id
    transactionService
      .delete(id)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
};
