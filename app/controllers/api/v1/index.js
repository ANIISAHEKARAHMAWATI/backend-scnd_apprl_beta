const userController = require("./userController");
const appController = require("./appController")
const productController = require("./productController")
const transactionController = require("./transactionController")
const notifController = require("./notifController")

module.exports = {
  userController,
  appController,
  productController,
  transactionController,
  notifController
};
