// const userService = require("../../../services/userService");
const bcrypt = require("bcryptjs");
const userService = require("../../../services/userService");
const jwt = require("jsonwebtoken") 
const cors = require("cors")
const upload = require("../../../cloudinary/upload")
const { uploadToCloudinary } = require("../../../cloudinary/cloudinary")
// const cloudinary = require("../../../cloudinary/cloudinary");
const fs = require("fs");
const {body, validationResult} = require('express-validator')
const SALT = 10;
const { extname } = require('path')

function encryptPassword(password) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, SALT, (err, encryptedPassword) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(encryptedPassword);
    });
  });
}

function checkPassword(encryptedPassword, password) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(isPasswordCorrect);
    });
  });
}

function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia", {expiresIn: '24h'});
}

module.exports = {
  //cek banyak user
  list(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );

    userService
      .list()
      .then(({ data, count }) => {
        res.status(200).json({
          status: "OK",
          data: { posts: data },
          meta: { total: count },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async create(req, res) {
    const errors = validationResult(req)

    try {
      if (!errors.isEmpty() && errors.errors[0].param === 'email') {
        return res.status(400)
        .json({
          message: "Invalid email address. Please try again.",
        });
      }
      if (!errors.isEmpty() && errors.errors[0].param === 'password') {
        return res
          .status(400)
          .json({
            message: "Password must be longer than 6 characters",
          });
      }
      const encryptedPassword = await encryptPassword(req.body.password);
      const password = encryptedPassword;
      const email = req.body.email;
      const userName = req.body.userName;
      const role = "buyer";
      const cek_duplicate = await userService.login(email)
      // console.log(cek_duplicate)
      if (cek_duplicate) {
        return res
          .status(400)
          .json({
            message: "Duplicate Email",
          });
      }


      // console.log(encryptPassword)
      const data = {userName, email, role, password}
      userService
        .create(data)
        .then((post) => {
          res.status(201).json({
            status: "SUCCESS REGISTER",
            data: {
              user_name : post.userName,
              email,
            }
          });
        })
        .catch((err) => {
          res.status(422).json({
            status: "FAIL",
            message: err.message,
          });
        });
    } catch (err) {
      console.error(err);
    }

    
  },


  async update(req, res) {
    // console.log(req.file)
    var locaFilePath, result, role, cekfile;
    let userImage, data;
    
    locaFilePath = req.file.path;
    cekfile = extname(locaFilePath).toString()
    // console.log(cekfile)
    if(cekfile != ".jpg"){
      if(cekfile != ".png"){
        if(cekfile != ".jpeg"){
          res.status(400).json({message: "Not image file"});
          fs.unlinkSync(locaFilePath);
          return;
        }
      }
    }
    // console.log(locaFilePath)
    result = await uploadToCloudinary(locaFilePath);
    role = "seller";
    userImage = result.url;
    const userName = req.body.userName;
    const address = req.body.address;
    const phone = req.body.phone;
    const city = req.body.city

    data = {userName, userImage, role, address, phone, city}
    
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    // console.log(tokenPayload)

    userService
      .update(tokenPayload.id, data)
    userService
      .checknew(tokenPayload.email)
      .then(({data}) => {
        console.log(data)
        const token = createToken({
          id: data.id,
          name: data.userName,
          image: data.userImage,
          email: data.email,
          role: data.role,
        });

        res.status(200).json({
          status: "SUCCESS UPDATE", 
          token
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  profile(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );

    userService
      .get(tokenPayload.id)
      .then((post) => {
        res.status(200).json({
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  destroy(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );

    if (tokenPayload.role == "buyer" || tokenPayload.role == "seller") {
      res.status(403).json({ message: "Tidak punya hak beb" });
      return;
    }

    userService
      .delete({
        where: {
          id: req.params.id
        }
      })
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async login(req, res) {
    const email = req.body.email.toLowerCase(); // Biar case insensitive
    let password2 = req.body.password;
    const loginuser = await userService.login(email)
    // console.log(loginuser)

    if (!loginuser) {
      res.status(404).json({ message: "Email not found" });
      return;
    }

    const isPasswordCorrect = await checkPassword(
      loginuser.password,
      password2
    );

    if (!isPasswordCorrect) {
      res.status(401).json({ message: "Wrong Password!" });
      return;
    }

    const token = createToken({
      id: loginuser.id,
      name: loginuser.userName,
      image: loginuser.userImage,
      email: loginuser.email,
      role: loginuser.role,
    });

    res.status(200).json({
      // status_login: "Berhasil",
      // email: loginuser.email,
      token, // Kita bakal ngomongin ini lagi nanti.
    });
  },

  async authorize(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );
      console.log(tokenPayload)

      req.loginuser = await userService.get(tokenPayload.id);
      next();
    } catch (err) {
      console.error(err);
      res.status(401).json({
        message: "Unauthorized",
      });
    }
  },
};
