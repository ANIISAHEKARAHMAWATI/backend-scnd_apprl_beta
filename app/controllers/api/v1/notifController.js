const jwt = require("jsonwebtoken") 
const notifService =require("../../../services/notifService");
const { hasNextPages } = require("express-paginate");

module.exports = { 
  list(req, res) {
    notifService
      .listall()
      .then(({ data }) => {
        res.status(200).json({
          status: "OK",
          data,
          meta: { 
            total: data.length 
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  // create transaction and create notif 
  async create(req, res) {
    try{
      // get data penawarann
      const buyerId = req.body.buyerId
      const sellerId = req.body.sellerId
      const transactionId = req.body.transactionId
      const productId = req.body.productId
      const message = req.body.message

      let data = { sellerId, buyerId, transactionId, productId, message}
    
        let upload = await notifService.create(data)
        // console.log(tokenPayload)
        res.status(201).json({
          status: "Penawaran Berhasil Dibuat",
          upload
        });
    }catch(err){
      res.status(422).json({
        status: "FAIL",
        message: err.message,
      });
    };
  },

  //by id
  show(req, res) {
    const buyerId = req.body.buyerId
    notifService
      .get(buyerId)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          post
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
};
