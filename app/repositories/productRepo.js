const { Product,User,Image_Product } = require("../models");
const { Op } = require("sequelize");

module.exports = {
  create(createArgs) {
    return Product.create(createArgs);
  },

  update(id, updateArgs) {
    return Product.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  // delete(id) {
  //   return Product.destroy(id);
  // },

  find(id) {
    return Product.findByPk(id,{
      attributes: ["id", ["userId", "user_id"], ["productName", "product_name"], ["productPrice", "product_price"],["productDesc", "product_desc"], ["productCategory", "product_category"] , ["productStatus", "product_status"]],
      include :[
        {
          model:User,
          attributes: ["id", ["userName", "user_name"],  ["userImage", "user_image"] ,"email" ,"role" ,"phone" ,"city" ,"address"],
          // as:"profile",
        },
        {
          model: Image_Product,
          attributes: ["id", "link"],
        }
      ],
    });
  },

  findAll(token) {
    return Product.findAll({
      where: {
        [Op.and]:{
          userId: token,
          productStatus: "available"
        }
      },
      order: [
        ['id', 'DESC'],
      ],
      attributes: ["id",  ["userId", "user_id"], ["productName", "product_name"], ["productPrice", "product_price"],["productDesc", "product_desc"], ["productCategory", "product_category"] , ["productStatus", "product_status"]],
      include :[
        {
          model:User,
          attributes: ["id", ["userName", "user_name"] , ["userImage", "user_image"],"email","role","phone","city","address"],
          // as:"profile",
        },
        {
          model: Image_Product,
          attributes: ["id", "link"],
        }
      ],
    });
  },

  cek(id){
    return Product.findOne({
      where: {
         id, 
         productStatus: "available",
      },
      attributes: ["id",  ["userId", "user_id"], ["productName", "product_name"], ["productPrice", "product_price"],["productDesc", "product_desc"], ["productCategory", "product_category"] , ["productStatus", "product_status"]],
      raw: true,
      nest: true,
    })
  },

  //filter nama
  search(productName, limit, page){
    let offset = limit*(page-1);
    let search = "%" + productName + "%"

    console.log(search)
    return Product.findAll({
      limit: limit, 
      offset,
      where : {
        productStatus: "available",
        productName:{
          [Op.like]: search,
        }
      },
      attributes: ["id",  ["userId", "user_id"], ["productName", "product_name"], ["productPrice", "product_price"],["productDesc", "product_desc"], ["productCategory", "product_category"] , ["productStatus", "product_status"]],
      include: [
        {
          model: Image_Product,
          attributes: ["id", "link"],
        }
      ]
    })
  },

  //filter category
  sort(productCategory, limit, page){
    let offset = limit*(page-1);
    return Product.findAll({
      limit: limit, 
      offset,
      where: {
        productStatus: "available",
        productCategory
      },
      order: [
        ['id', 'DESC'],
      ],
      attributes: ["id",  ["userId", "user_id"], ["productName", "product_name"], ["productPrice", "product_price"],["productDesc", "product_desc"], ["productCategory", "product_category"] , ["productStatus", "product_status"]],
      include: [
        {
          model: Image_Product,
          attributes: ["id", "link"],
        }
      ]
    })
  },

  // filter semua
  filterAll(search, category, limit, page){
    let offset = limit*(page-1);
    let newsearch = "%" + search + "%"
    return Product.findAll({
      limit: limit, 
      offset,
      where: {
        productStatus: "available",
        productName:{
          [Op.like]: newsearch,
        },
        productCategory :category,
      },
      order: [
        ['id', 'DESC'],
      ],
      attributes: ["id",  ["userId", "user_id"], ["productName", "product_name"], ["productPrice", "product_price"],["productDesc", "product_desc"], ["productCategory", "product_category"] , ["productStatus", "product_status"]],
      include :[
        {
          model:User,
          attributes: ["id", ["userName", "user_name"] , ["userImage", "user_image"],"email","role","phone","city","address"],
          // as:"profile",
        },
      ],
    
    })
  },

  // Untuk menampilkan semua data tanpa authorized (clear)
  allData(limit, page){
    let offset = limit*(page-1);
    // console.log(limit)
    // console.log(page)
    return Product.findAll({
      order: [
        ['id', 'DESC'],
      ],
      limit: limit, 
      offset,
      attributes: ["id",  ["userId", "user_id"], ["productName", "product_name"], ["productPrice", "product_price"],["productDesc", "product_desc"], ["productCategory", "product_category"] , ["productStatus", "product_status"]],
      include: [
        {
          model: Image_Product,
          attributes: ["id", "link"],
        }
      ]
    })
  },

  history(token){
    return Product.findAll({
      where: {
        productStatus: "soldout",
        [Op.and]:{
          userId: token,
        }
      },
      attributes: ["id",  ["userId", "user_id"], ["productName", "product_name"], ["productPrice", "product_price"],["productDesc", "product_desc"], ["productCategory", "product_category"] , ["productStatus", "product_status"]],
      include :[
        {
          model:User,
          attributes: ["id", ["userName", "user_name"] , ["userImage", "user_image"],"email","role","phone","city","address"],
          // as:"profile",
        },
        {
          model: Image_Product,
          attributes: ["id", "link"],
        }
      ],
    });
  }
};
