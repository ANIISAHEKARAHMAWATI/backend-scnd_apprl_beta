const { Image_Product,User } = require("../models");
const { Op } = require("sequelize");

module.exports = {
  create(createArgs) {
    return Image_Product.create(createArgs);
  },

  update(id, updateArgs) {
    return Image_Product.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return Image_Product.destroy(id);
  },
};
