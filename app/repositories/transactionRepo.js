const { User_Transaction, Product, User, Image_Product } = require("../models");
const { Op } = require("sequelize");

module.exports = {
  create(createArgs) {
    // console.log(createArgs)
    return User_Transaction.create(createArgs);
  },

  update(id, updateArgs) {
    return User_Transaction.update(updateArgs, {
      where: {
        id
      },
    });
  },

  delete(id) {
    // console.log(id)
    return User_Transaction.destroy({where:{id}});
  },

  find(id) {
    return User_Transaction.findByPk(id, {
      attributes: ["id", ["productId", "product_id"] , ["buyerId","buyer_id"] , ["sellerId","seller_id"] , ["priceNegotiate", "price_negotiate"] , ["transactionStatus", "transaction_status"]],
      include: [
        {
          model: Product,
          attributes: ["id", ["productName", "product_name"], ["productPrice", "product_price"], ["productDesc", "product_desc"], ["productCategory", "product_category"], ["productStatus", "product_status"]],
          include:[
            {
              model: Image_Product,
              attributes: ["id", "link"],
            },
            {
              model: User,
              attributes: ["id", ["userName", "user_name"], ["userImage", "user_image"]],
              // as:"profile",
            },
          ]
        },
        {
          model: User,
          attributes: ["id", ["userName", "user_name"], ["userImage", "user_image"]],
        },
      ],
    });
  },

  findAll(limit, page) {
    let offset = limit*(page-1);
    // console.log(pid)
    return User_Transaction.findAll({
      limit: limit, 
      offset,
      attributes: ["id",["productId", "product_id"], ["buyerId","buyer_id"] , ["sellerId","seller_id"] , ["priceNegotiate", "price_negotiate"], ["transactionStatus", "transaction_status"]],
      include: [
        {
          model: Product,
          attributes: ["id",  ["productName", "product_name"], ["productPrice", "product_price"], ["productDesc", "product_desc"], ["productCategory", "product_category"], ["productStatus", "product_status"]],
          include: [
            {
              model: Image_Product,
              attributes: ["id", "link"],
            }
          ]// as:"products",
        },
        {
          model: User,
          attributes: ["id", ["userName", "user_name"], ["userImage", "user_image"]],
          // as:"profile",
        },
      ],
    });
  },

  listTransaction(userId){
    return User_Transaction.findAll({
      where: {
        [Op.or]:{
          buyerId: userId,
          sellerId: userId
        }
      }, 
      attributes: ["id", ["productId", "product_id"], ["buyerId","buyer_id"] , ["sellerId","seller_id"] , ["priceNegotiate", "price_negotiate"], ["transactionStatus", "transaction_status"]],
      include: [
        {
          model: Product,
          attributes: ["id",  ["productName", "product_name"], ["productPrice", "product_price"], ["productDesc", "product_desc"], ["productCategory", "product_category"], ["productStatus", "product_status"]],
          include: [
            {
              model: Image_Product,
              attributes: ["id", "link"],
            }
          ],
        },
        {
          model: User,
          attributes: ["id", ["userName", "user_name"], ["userImage", "user_image"]],
          // as:"profile",
        },
      ],
    })
  },

  cek(id){
    return User_Transaction.findOne({
      where: {
         id,
      },
      attributes: ["id", ["productId", "product_id"], ["buyerId","buyer_id"] , ["sellerId","seller_id"] , ["priceNegotiate", "price_negotiate"], ["transactionStatus", "transaction_status"]],
      raw: true,
      nest: true,
    })
  }
};
