const { User } = require("../models");


module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  login(email){
    return User.findOne({
      where: { email },
    })
  },


  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return User.destroy(id);
  },

  find(id) {
    return User.findByPk(id,{
      attributes: [["userName", "user_name"], ["userImage", "user_image"] ,"email" ,"phone" ,"city" ,"address"],
    });
  },

  findAll() {
    return User.findAll();
  },

  getTotalPost() {
    return User.count();
  },
};
