const { User_Transaction, Product, User, Image_Product, Notif } = require("../models");
const { Op } = require("sequelize");

module.exports = {
  create(createArgs) {
    return Notif.create(createArgs);
  },

  find(id) {
    return Notif.findOne({
      where:{
        buyerId: id,
      },
      attributes: ["id",["sellerId","seller_id"] ,["buyerId","buyer_id"] ,["productId", "product_id"],   ["transactionId", "transaction_id"], "message"],
      include: [
        {
          model: User_Transaction,
          attributes: ["id", ["productId", "product_id"], ["buyerId","buyer_id"] , ["sellerId","seller_id"] , ["priceNegotiate", "price_negotiate"], ["transactionStatus", "transaction_status"]],
          include: [
            {
              model: Product,
              attributes: ["id",  ["productName", "product_name"], ["productPrice", "product_price"], ["productDesc", "product_desc"], ["productCategory", "product_category"], ["productStatus", "product_status"]],
              include: [
                {
                  model: User,
                  attributes: ["id", ["userName", "user_name_seller"], ["userImage", "user_image"]],
                  // as:"profile",
                },
                {
                  model: Image_Product,
                  attributes: ["id", "link"],
                },
              ],
            },
          ]// as:"products",
        },
        {
          model: User,
          attributes: ["id", ["userName", "user_name_buyer"], ["userImage", "user_image"]],
          // as:"profile",
        },
      ],
    });
  },

  findAll() {
    return Notif.findAll({
      attributes: ["id",["sellerId","seller_id"] ,["buyerId","buyer_id"] ,["productId", "product_id"],   ["transactionId", "transaction_id"], "message"],
      include: [
        {
          model: User_Transaction,
          attributes: ["id", ["productId", "product_id"], ["buyerId","buyer_id"] , ["sellerId","seller_id"] , ["priceNegotiate", "price_negotiate"], ["transactionStatus", "transaction_status"]],
          include: [
            {
              model: Product,
              attributes: ["id",  ["productName", "product_name"], ["productPrice", "product_price"], ["productDesc", "product_desc"], ["productCategory", "product_category"], ["productStatus", "product_status"]],
              include: [
                {
                  model: User,
                  attributes: ["id", ["userName", "user_name_seller"], ["userImage", "user_image"]],
                  // as:"profile",
                },
                {
                  model: Image_Product,
                  attributes: ["id", "link"],
                },
              ],
            },
          ]// as:"products",
        },
        {
          model: User,
          attributes: ["id", ["userName", "user_name_buyer"], ["userImage", "user_image"]],
          // as:"profile",
        },
      ],
    });
  },

};
