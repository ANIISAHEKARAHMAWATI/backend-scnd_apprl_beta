/**
 * @file Bootstrap express.js server
 * @author Fikri Rahmat Nurhidayat
 */
// 
const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const { MORGAN_FORMAT } = require("./application")
const router = require("../config/routes");

const app = express();

/** Install request logger */
app.use(morgan(MORGAN_FORMAT));

/** Install JSON request parser */
app.use(cors())
app.use(express.json());

/** Install Router */
app.use(router);

module.exports = app;