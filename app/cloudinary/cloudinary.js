const cloudinary = require("cloudinary").v2
const fs = require("fs");

cloudinary.config(
    {
        cloud_name: "leozan-8090",
        api_key: "896739596871519",
        api_secret: "cN36_Q1G0n38gWV71sBrS2uXddU",
        secure: true,
    }
)

async function uploadToCloudinary(locaFilePath) {
    return cloudinary.uploader
        .upload(locaFilePath)
        .then((result) => {
  
            // Image has been successfully uploaded on
            // cloudinary So we dont need local image 
            // file anymore
            // Remove file from local uploads folder
            fs.unlinkSync(locaFilePath);
  
            return {
                message: "Success",
                url: result.url,
            };
        })
        .catch((error) => {
  
            // Remove file from local uploads folder
            fs.unlinkSync(locaFilePath);
            return { message: "Fail" };
        });
}

module.exports = {uploadToCloudinary};