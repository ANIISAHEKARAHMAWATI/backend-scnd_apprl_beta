'use strict';


const productNames = [
  "Casio AE-1000W-1AVDF",
  "Roughneck H264 Sig",
  "Casio MRW-200H-1B2VDF",
  "Adidas ULTRABOOST 21 Men's",
  "Casio  MW-600F-7AVDF",
  "Puma NRGY Comet Men's Running Shoes",
  "Roughneck SS256 ",
  "BODYPACK INSIGHT SHOULDER BAG BLACK",
  "EIGER EMISSARY 3L WAIST BAG",
];

const Category = ["Jam", "Baju", "Celana", "Yang Diminati", "Sepatu"];

module.exports = {
  async up(queryInterface, Sequelize) {

    function generateRandomId(number) {
      return Math.floor(Math.random() * number) + 1;
    }

    const product = [];
    Category.forEach((productCategory) => {
      product.push(
        ...productNames.map((name, i) => {
          const accumulator = i.toLocaleString('en-US', {
            minimumIntegerDigits: 2,
            useGrouping: false
          });

          const timestamp = new Date();

          return ({
            productImage: `https://images.pexels.com/photos/280250/pexels-photo-280250.jpeg?cs=srgb&dl=pexels-pixabay-280250.jpg&fm=jpg`,
            userId: generateRandomId(15),
            // userId: 1,
            productName: name,
            productPrice: 100000,
            productCategory: productCategory,
            productDesc: "Labore ut eiusmod amet officia elit exercitation eiusmod ad aliqua aliquip culpa aute. Laboris id consectetur amet veniam quis duis velit exercitation ullamco velit elit dolore enim. Et non consequat proident sint officia.",
            productStatus: "available",
            createdAt: timestamp,
            updatedAt: timestamp,
          })
        })
      )
    })
    await queryInterface.bulkInsert('Products', product, {})
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Products', null, {});
  }
};
