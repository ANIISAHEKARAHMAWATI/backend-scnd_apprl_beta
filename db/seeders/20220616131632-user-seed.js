'use strict';

const { Op } = require("sequelize");
const bcrypt = require("bcryptjs");
// const { Role } = require("../app/models");

const userName = [
  "Mr. Example",
  "Mr. HelloWorld",
  "Mrs. Dummy",
  "Mrs. Jane",
  "Alexmo Swagger",
  "Alfred Benjamin",
  "Adit",
  "Rachma210",
  "Slavoski Zylech",
  "Console1000",
  "abcd0000",
  "Whoami",
  "Noob Master 90",
  "Tok Dalang",
  "Lucinta Luna"
];


module.exports = {
  async up(queryInterface, Sequelize) {
    const password = "12345";
    const encryptedPassword = bcrypt.hashSync(password, 10);
    const timestamp = new Date();

    const users = userName.map((userName) => ({
      userName,
      email: `${userName.toLowerCase().replace(/\s+/g, '')}@gmail.com`,
      password: encryptedPassword,
      role: "Buyer",
      userImage: "https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?cs=srgb&dl=pexels-daniel-xavier-1239291.jpg&fm=jpg",
      createdAt: timestamp,
      updatedAt: timestamp,
    }))

    await queryInterface.bulkInsert('Users', users, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', { userName: { [Op.in]: userName } }, {});
  }
};
