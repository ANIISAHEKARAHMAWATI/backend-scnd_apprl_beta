'use strict';

const userId = [1, 2, 3]
const productId = [3, 4, 5]

module.exports = {
  async up(queryInterface, Sequelize) {
    const setTransaction = []

    productId.forEach((product) => {
      setTransaction.push(
        ...userId.map((idUser, i) => {
          const accumulator = i.toLocaleString('en-US', {
            minimumIntegerDigits: 2,
            useGrouping: false
          });


          const timestamp = new Date();

          return ({
            productId: product,
            userId: idUser,
            priceNegotiate: 200000,
            transactionStatus: "waiting",
            createdAt: timestamp,
            updatedAt: timestamp,
          })
        })
      )
    })

    await queryInterface.bulkInsert('User_Transactions', setTransaction, {})
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('User_Transactions', null, {});
  }
};
